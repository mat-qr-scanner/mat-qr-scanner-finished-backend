<?php


include "../connection/connection.php";
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Access-Control-Allow-Origin: *');

$input=file_get_contents("php://input");
$decode=json_decode($input, true);

$secretKey = $decode['secretKey'];
if(!isset($secretKey)){
    echo '{"message":"unauthorized"}';
    
    // header('location:http://google.com');
    return;
}else {
    if($secretKey !== "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55IjoiTUFUZWNoIiwiZGV2ZWxvcGVyIjoiU2hlcndpbiBCYXV0aXN0YSIsImVtcElEIjoxMDk5MH0.uel5E-cu8H-AKBzP5kqMNwjc65VaDHo90hl31CgVgjY"){
        echo '{"message":"unauthorized"}';
        // header('location:http://google.com');
        
        return;
    }
}
// $qrData = array();
// $qrData = $decode['qrData'];
$qrData = $decode['qrData'];
$empNum = $decode['empNum'];

$qrDataPO = $qrData[0]['PO'];
$qrDataGOODSCODE = $qrData[0]['GOODS_CODE'];
$qrDataINVOICE = $qrData[0]['INVOICE'];
$qrDataDATERECEIVE = $qrData[0]['DATE_RECEIVE'];
$qrDataQTY = $qrData[0]['QTY'];
$qrDataASYLINE = $qrData[0]['ASY_LINE'];
$qrDataSUPPLIER = $qrData[0]['SUPPLIER'];
$qrDataPARTNUMBER = $qrData[0]['PART_NUMBER'];
$qrDataNUMBEROFBOX = $qrData[0]['NUMBER_OF_BOX'];
$resultArray = array();

$query = sqlsrv_query( $localConn, "SELECT * FROM [Warehouse_Traceability_Scanner] 
WHERE INVOICE ='$qrDataINVOICE'
AND DATE_RECEIVE = '$qrDataDATERECEIVE'
AND GOODS_CODE = '$qrDataGOODSCODE'
AND QTY = '$qrDataQTY'
AND PO = '$qrDataPO'
AND ARCHIVE = '0'", array());

if ($query !== NULL){
    $rows = sqlsrv_has_rows( $query );

    if($rows === true){
        // sleep(5);
        echo '{"message":"duplicate"}';
        return;
    }
    $insertQuery = "INSERT INTO [MA_Receiving].[dbo].[Warehouse_Traceability_Scanner](GOODS_CODE, ASSY_LINE, SUPPLIER, PART_NUMBER, QTY, DATE_RECEIVE, INVOICE, ARCHIVE, PO, NUMBER_OF_BOX, LOT_NUMBER, SCANNER_EMP_ID)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
    $params = array(
        $qrDataGOODSCODE, 
        $qrDataASYLINE, 
        $qrDataSUPPLIER, 
        $qrDataPARTNUMBER, 
        $qrDataQTY, 
        $qrDataDATERECEIVE,
        $qrDataINVOICE,
        '0',
        $qrDataPO,
        $qrDataNUMBEROFBOX,
        $qrData[1]['LOT_NUMBER'],
        $empNum);

    $stmt = sqlsrv_query( $localConn, $insertQuery, $params);
    // sleep(5);
    if( $stmt ){
        $query2 = sqlsrv_query( $localConn, "SELECT * FROM [Warehouse_Traceability_Scanner] 
        WHERE INVOICE ='$qrDataINVOICE'
        AND DATE_RECEIVE = '$qrDataDATERECEIVE'
        AND GOODS_CODE = '$qrDataGOODSCODE'
        AND QTY = '$qrDataQTY'
        AND PO = '$qrDataPO'
        AND ARCHIVE = '0'", array());

        if ($query2 !== NULL){
        $rows2 = sqlsrv_has_rows( $query2 );

            if($rows2 === true){
                
                // sleep(5);
                $i = 0;
                    while($row2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_ASSOC)) {
                    
                    // echo $row['PO'];
                    // echo json_encode($resultArray, JSON_PRETTY_PRINT) . "<br>";
                    // $resultArray[$i]['message'] = "confirm";
                    $resultArray[$i]['ID'] = $row2['ID'];
                    $resultArray[$i]['message'] = 'success';
                    }
                    echo json_encode($resultArray, JSON_PRETTY_PRINT);
                return;
            }

    
        // echo json_encode(data[message] = "success"}, JSON_PRETTY_PRINT);
        }
                                
        else
        {
            echo '{"message":"failed"}';
        }
    }
}

    // echo json_encode($resultArray, JSON_PRETTY_PRINT);
    // echo json_encode($resultArray[0], JSON_PRETTY_PRINT);

sqlsrv_close($localConn);


